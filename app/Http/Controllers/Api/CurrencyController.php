<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Rules\ValidateCurrency;
use App\Services\CurrencyService;

class CurrencyController extends Controller
{
    public function __construct(CurrencyService $currency_service)
    {
        $this->currency_service = $currency_service;
    }

    public function conversion(Request $request)
    {
        $request_data = $request->all();

        try {

            $valid_currency = $this->currency_service->getValidCurrency();

            Validator::make($request_data, [
                'source_currency' => ['required', new ValidateCurrency($valid_currency, '來源幣別')],
                'target_currency' => ['required', new ValidateCurrency($valid_currency, '目標幣別')],
                'price' => ['required', 'numeric'],
            ], [
                'source_currency.required' => '請輸入{來源幣別}',
                'target_currency.required' => '請輸入{目標幣別}',
                'price.required' => '請輸入{金額數字}',
                'price.numeric' => '{金額數字}必須為數字格式',
            ])->validate();

            $conversion_price = $this->currency_service->getCurrencyConversionPrice($request_data['source_currency'], $request_data['target_currency'], $request_data['price']);

            return api_response('0000', ['conversion_price' => $conversion_price]);

        } catch (\Illuminate\Validation\ValidationException $e) {

            return api_response('9998', [], validate_error_msg($e->errors()));

        } catch (\Exception $e) {

            return api_response('9998', [], $e->getMessage());
            
        }
    }
}
