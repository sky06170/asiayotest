<?php

if (!function_exists('api_response')) {
    /**
     * 取得 api response 的 json string
     * 
     * @param string $code 錯誤代碼
     * @param array $data 回傳資料
     * @param string $message 錯誤訊息
     * @return array
     */
    function api_response($code = '', $data = [], $message = '')
    {
        $def = [
            '0000' => 'SUCCESS',
            '0001' => '未知錯誤',
            '9999' => '參數遺失',
            '9998' => '資料讀取失敗'
        ];

        $output = [
            'code' => $code,
            'data' => $data,
            'message' => (empty($message) ? $def[$code] : $message)
        ];

        return response()->json($output);
    }
}

if (!function_exists('validate_error_msg')) {
    /**
     * 取得驗證請求產生的錯誤訊息
     *
     * @param array $errors
     * @return string
     */
    function validate_error_msg($errors = [])
    {
        foreach ($errors as $error) {
            return $error[0];
        }
    }
}