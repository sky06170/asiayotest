<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateCurrency implements Rule
{
    public function __construct($valid_currency = [], $currency_text = '')
    {
        $this->valid_currency = $valid_currency;
        $this->currency_text = $currency_text;
    }
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return in_array($value, $this->valid_currency) ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "請輸入正確的{$this->currency_text}{" . implode(', ', $this->valid_currency) . "}";
    }
}
