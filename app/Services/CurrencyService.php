<?php

namespace App\Services;

class CurrencyService
{
    /**
     * 取得貨幣轉換後的金額
     *
     * @param string $source
     * @param string $target
     * @param integer $price
     * @return string
     */
    public function getCurrencyConversionPrice($source = '', $target = '', $price = 0)
    {
        $valid_currency = $this->getValidCurrency();

        if (!in_array($source, $valid_currency)) {
            throw new \Exception('錯誤的來源幣別');
        }

        if (!in_array($target, $valid_currency)) {
            throw new \Exception('錯誤的目標幣別');
        }

        if (!is_numeric($price)) {
            throw new \Exception('錯誤的金額數字格式');
        }

        $target_currency_rate = $this->getTargetCurrencyRate($source, $target);

        return number_format(round($target_currency_rate * $price, 2), 2);
    }

    private function getTargetCurrencyRate($source = '', $target = '')
    {
        $currencies = [
            'TWD' => [
                'TWD' => 1,
                'JPY' => 3.669,
                'USD' => 0.03281
            ],
            'JPY' => [
                'TWD' => 0.26956,
                'JPY' => 1,
                'USD' => 0.00885
            ],
            'USD' => [
                'TWD' => 30.444,
                'JPY' => 111.801,
                'USD' => 1
            ],
        ];

        return $currencies[$source][$target];
    }

    public function getValidCurrency()
    {
        return ['TWD', 'JPY', 'USD'];
    }
}