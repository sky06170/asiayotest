<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CurrencyTest extends TestCase
{   
    /**
     * 測試：取得幣別轉換金額 API
     * 情境：TWD 轉 USD
     * 狀態：成功
     */
    public function testConversionSuccessByTWDToUSD()
    {
        $request_data = http_build_query([
            'source_currency' => 'TWD',
            'target_currency' => 'USD',
            'price' => 1000,
        ]);

        $response = $this->get('/api/v1/currency/conversion?' . $request_data);

        $response
            ->assertJson([
                'code' => '0000',
                'data' => [
                    'conversion_price' => "32.81"
                ]
            ]);
    }

    /**
     * 測試：取得幣別轉換金額 API
     * 情境：JPY 轉 TWD
     * 狀態：成功
     */
    public function testConversionSuccessByJPYToTWD()
    {
        $request_data = http_build_query([
            'source_currency' => 'JPY',
            'target_currency' => 'TWD',
            'price' => 10000,
        ]);

        $response = $this->get('/api/v1/currency/conversion?' . $request_data);

        $response
            ->assertJson([
                'code' => '0000',
                'data' => [
                    'conversion_price' => "2,695.60"
                ]
            ]);
    }

    /**
     * 測試：取得幣別轉換金額 API
     * 情境：提供錯誤的來源幣別
     * 狀態：失敗
     */
    public function testConversionFailByEmptySource()
    {
        $request_data = http_build_query([
            'target_currency' => 'TWD',
            'price' => 10000,
        ]);

        $response = $this->get('/api/v1/currency/conversion?' . $request_data);

        $response
            ->assertJson([
                'code' => '9998',
                'message' => '請輸入{來源幣別}'
            ]);
    }

    /**
     * 測試：取得幣別轉換金額 API
     * 情境：提供錯誤的目標幣別
     * 狀態：失敗
     */
    public function testConversionFailByEmptyTarget()
    {
        $request_data = http_build_query([
            'source_currency' => 'JPY',
            'price' => 10000,
        ]);

        $response = $this->get('/api/v1/currency/conversion?' . $request_data);

        $response
            ->assertJson([
                'code' => '9998',
                'message' => '請輸入{目標幣別}'
            ]);
    }

    /**
     * 測試：取得幣別轉換金額 API
     * 情境：提供錯誤的金額數字格式
     * 狀態：失敗
     */
    public function testConversionFailByExceptionFormatPrice()
    {
        $request_data = http_build_query([
            'source_currency' => 'JPY',
            'target_currency' => 'TWD',
            'price' => 'abcde',
        ]);

        $response = $this->get('/api/v1/currency/conversion?' . $request_data);

        $response
            ->assertJson([
                'code' => '9998',
                'message' => '{金額數字}必須為數字格式'
            ]);
    }
}
