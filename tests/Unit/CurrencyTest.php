<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\CurrencyService;

class CurrencyTest extends TestCase
{
    public function setUp(): void
    {
        $this->currency_service = new CurrencyService();
    }

    /**
     * 測試：取得幣別轉換金額
     * 情境：TWD 轉 USD
     * 狀態：成功
     */
    public function testGetCurrencyConversionPriceSuccessByTWDToUSD()
    {
        $source = 'TWD';
        $target = 'USD';
        $price = 1000;
        $estimate_price = '32.81';

        try {

            $conversion_price = $this->currency_service->getCurrencyConversionPrice($source, $target, $price);

        } catch (\Exception $e) {

            $conversion_price = '';

        }

        $this->assertEquals($conversion_price, $estimate_price);
    }

    /**
     * 測試：取得幣別轉換金額
     * 情境：JPY 轉 TWD
     * 狀態：成功
     */
    public function testGetCurrencyConversionPriceSuccessByJPYToTWD()
    {
        $source = 'JPY';
        $target = 'TWD';
        $price = 10000;
        $estimate_price = '2,695.60';

        try {

            $conversion_price = $this->currency_service->getCurrencyConversionPrice($source, $target, $price);

        } catch (\Exception $e) {

            $conversion_price = $e->getMessage();

        }

        $this->assertEquals($conversion_price, $estimate_price);
    }

    /**
     * 測試：取得幣別轉換金額
     * 情境：提供錯誤的來源幣別
     * 狀態：失敗
     */
    public function testGetCurrencyConversionPriceFailByEmptySource()
    {
        $source = '';
        $target = 'USD';
        $price = 1000;
        $error_msg = '';

        try {

            $this->currency_service->getCurrencyConversionPrice($source, $target, $price);

        } catch (\Exception $e) {

            $error_msg = $e->getMessage();

        }

        $this->assertEquals($error_msg, '錯誤的來源幣別');
    }

    /**
     * 測試：取得幣別轉換金額
     * 情境：提供錯誤的目標幣別
     * 狀態：失敗
     */
    public function testGetCurrencyConversionPriceFailByEmptyTarget()
    {
        $source = 'TWD';
        $target = '';
        $price = 1000;
        $error_msg = '';

        try {

            $this->currency_service->getCurrencyConversionPrice($source, $target, $price);

        } catch (\Exception $e) {

            $error_msg = $e->getMessage();

        }

        $this->assertEquals($error_msg, '錯誤的目標幣別');
    }

    /**
     * 測試：取得幣別轉換金額
     * 情境：提供錯誤的金額數字格式
     * 狀態：失敗
     */
    public function testGetCurrencyConversionPriceFailByExceptionFormatPrice()
    {
        $source = 'TWD';
        $target = 'USD';
        $price = 'abcde';
        $error_msg = '';

        try {

            $this->currency_service->getCurrencyConversionPrice($source, $target, $price);

        } catch (\Exception $e) {

            $error_msg = $e->getMessage();

        }

        $this->assertEquals($error_msg, '錯誤的金額數字格式');
    }
}
